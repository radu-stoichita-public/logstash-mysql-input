# Logstash Mysql Input Plugin

This is a Java plugin for [Logstash](https://github.com/elastic/logstash). It is an alternative to official JDBC input plugin
and differs in the workflow and features.

## License
It is fully free and fully open source. The license is Apache 2.0, meaning you are free to use it however you want.

The documentation for Logstash Java plugins is available [here](https://www.elastic.co/guide/en/logstash/6.7/contributing-java-plugin.html).

## Requirements

1. First __clone Logstash github repo__ in another directory as Logstash core is required to build this plugin :

    ```bash
    git clone https://github.com/elastic/logstash.git
    ```

2. Checkout the correct Logstash branch inside the logstash folder :

    ```bash
   git checkout 7.6 
   ```

3. Install __Java JDK__ and set JAVA_HOME in you __.bashrc__ if not already exists :

    Example (change accordingly to your Java setup)

    ```bash
    sudo apt install openjdk-14-jdk
    echo 'JAVA_HOME=/usr/lib/jvm/java-14-openjdk-amd64/' >> ~/.bashrc
    ```

4. Install jRuby :

    ```bash
    sudo apt-get install jruby
    ```

5. In the Logstash project remove deprecation from Java compile options :

    Original code in build.gradle :
    
    ```text
    tasks.withType(JavaCompile).configureEach {
      options.compilerArgs.add("-Xlint:all")
      ...
    }
    ```
   
    change to : 
    
    ```text
    tasks.withType(JavaCompile).configureEach {
      options.compilerArgs.add("-Xlint:-deprecation")
      ...
    }
    ```

6. Launch the assembly of Logstash in its own directory and go get a coffee :
    
    ```bash
    ./gradlew assemble
    ```
   
7. Install Logstash in your system by following the official guide : 

    https://www.elastic.co/guide/en/logstash/current/installing-logstash.html
    
    This step is not mandatory but you will need a local install to perform local tests.

## Plugin configuration

1. Set the logstash_core directory in file __gradle.properties__ (use dist) :

    ex : LOGSTASH_CORE_PATH=../logstash/logstash-core/
    
2. If your pipeline use ${PARAM} parameters, use the _.env.dist__ file to set them as a copy to .env

## Usage

Note : all scripts are __in root project directory__.

### Rebuild

If you need to rebuild the plugin, launch this plugin to get a clean and updated GEM build :

```bash
./rebuild.sh
```

### Normal build

If you need to build the plugin without clean rebuilding, launch this command to 
get an updated GEM build :

```bash
./build.sh
```

### Normal build then install

If you need to build the plugin without rebuilding then install it as plugin, launch this command to 
get an updated GEM build :

```bash
./build_install.sh
```

### Testing a pipeline without logging

Create a pipeline file base on contents of __pipeline.logstash.conf.dist__ and then launch the test :

```bash
./tesh.sh --pipeline=[REQUIRED test_pipeline_file_name ] --log-file=[OPTIONAL test.log]
```

### More explanations

TBD