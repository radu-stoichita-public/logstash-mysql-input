#!/bin/bash
set -e
./gradlew gem
sudo -u logstash /usr/share/logstash/bin/logstash-plugin install --no-verify --local ./logstash-input-mysql_input-1.0.0.gem
export $(grep -v '^#' .env | xargs -d '\n')