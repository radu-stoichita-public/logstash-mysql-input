#!/bin/bash
set -e

#### Arguments checking
function usage()
{
    echo ""
    echo "Test a pipeline file"
    echo ""
    echo "Usage :"
    echo "test.sh --help"
    echo ""
    echo "Required :"
    echo "--pipeline=test.pipeline.logstash.conf    Pipeline file to process"
    echo ""
    echo "Optional :"
    echo "--log-file=test.log    Optional tee to log specified file"
    echo ""
}

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit 0
            ;;
        --pipeline)
            PIPELINE=$VALUE
            ;;
        --log-file)
            LOGFILE=$VALUE
            ;;

        *)  printf "ERROR: unknown parameter \"$PARAM\"\n\n"
            usage
            exit 1
            ;;

    esac
    shift
done

REQUIRED_PARAMS=(PIPELINE)
MISSING_PARAM=0
for param in ${REQUIRED_PARAMS[*]}
do
    if [[ -z ${!param} ]]; then
        MISSING_PARAM=1
        printf "\nArgument [$param] is wrong or missing"
    fi
done
if [[ $MISSING_PARAM = 1 ]]; then
    echo ""
    usage
    exit 1
fi

SECRETS=$(echo $(grep -v '^#' .env))

if [ -n "$LOGFILE" ]; then
  echo -n > $LOGFILE
  sudo $SECRETS -u logstash /usr/share/logstash/bin/logstash --path.settings=/etc/logstash -f $PIPELINE | tee $LOGFILE
else
  sudo $SECRETS -u logstash /usr/share/logstash/bin/logstash --path.settings=/etc/logstash -f $PIPELINE
fi


