package fr.stoichita.logstash.mysql.input;

public class ResultsCount {

    private long count;
    private long min;
    private long max;

    public ResultsCount(long count, long min, long max) {
        this.count = count;
        this.min = min;
        this.max = max;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getMin() {
        return min;
    }

    public void setMin(long min) {
        this.min = min;
    }

    public long getMax() {
        return max;
    }

    public void setMax(long max) {
        this.max = max;
    }

}
