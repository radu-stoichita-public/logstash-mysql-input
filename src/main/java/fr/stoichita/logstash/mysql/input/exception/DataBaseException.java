package fr.stoichita.logstash.mysql.input.exception;

public class DataBaseException extends Exception {
    public DataBaseException(String msg) {
        super(msg);
    }
}
