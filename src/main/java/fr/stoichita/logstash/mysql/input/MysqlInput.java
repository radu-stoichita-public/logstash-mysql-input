package fr.stoichita.logstash.mysql.input;

import co.elastic.logstash.api.Configuration;
import co.elastic.logstash.api.Context;
import co.elastic.logstash.api.Input;
import co.elastic.logstash.api.LogstashPlugin;
import co.elastic.logstash.api.PluginConfigSpec;
import fr.stoichita.logstash.mysql.input.exception.DataBaseException;
import fr.stoichita.logstash.mysql.input.exception.StateManagerException;
import fr.stoichita.logstash.mysql.input.exception.TrackingException;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

@LogstashPlugin(name="mysql_input")

public class MysqlInput implements Input {

    // Configuration
    private String host;
    public static final PluginConfigSpec<String> CONFIG_HOST =
            PluginConfigSpec.stringSetting("host", null);

    private String port;
    public static final PluginConfigSpec<String> CONFIG_PORT =
            PluginConfigSpec.stringSetting("port", null);

    private String user;
    public static final PluginConfigSpec<String> CONFIG_USER =
            PluginConfigSpec.stringSetting("user", null);

    private String password;
    public static final PluginConfigSpec<String> CONFIG_PASSWORD =
            PluginConfigSpec.stringSetting("password", null);

    private String dbName;
    public static final PluginConfigSpec<String> CONFIG_DBNAME =
            PluginConfigSpec.stringSetting("database", null);

    private String options;
    public static final PluginConfigSpec<String> CONFIG_OPTIONS =
            PluginConfigSpec.stringSetting("options", null);

    private String count_statement;
    public static final PluginConfigSpec<String> CONFIG_COUNT_STATEMENT =
            PluginConfigSpec.stringSetting("count_statement", null);

    private String statement;
    public static final PluginConfigSpec<String> CONFIG_STATEMENT =
            PluginConfigSpec.stringSetting("statement", null);

    private String page_column;
    public static final PluginConfigSpec<String> CONFIG_PAGE_COLUMN =
            PluginConfigSpec.stringSetting("page_column", null);

    private Long page_size;
    public static final PluginConfigSpec<Long> CONFIG_PAGE_SIZE =
            PluginConfigSpec.numSetting("page_size", 1000);

    private Long sleep;
    public static final PluginConfigSpec<Long> CONFIG_SLEEP =
            PluginConfigSpec.numSetting("sleep", 60);

    private Long sleep_page;
    public static final PluginConfigSpec<Long> CONFIG_SLEEP_PAGE =
            PluginConfigSpec.numSetting("sleep_page", 5);

    private Boolean clean_run;
    public static final PluginConfigSpec<Boolean> CONFIG_CLEAN_RUN =
            PluginConfigSpec.booleanSetting("clean_run", false);

    private String state_path;
    public static final PluginConfigSpec<String> CONFIG_STATE_PATH =
            PluginConfigSpec.stringSetting("state_path", null);

    private String show_in_log;
    public static final PluginConfigSpec<String> CONFIG_SHOW_IN_LOG =
            PluginConfigSpec.stringSetting("show_in_log", null);

    private Map<String, Object> tracking;
    public static final PluginConfigSpec<Map<String, Object>> CONFIG_TRACKING =
            PluginConfigSpec.hashSetting("tracking");

    private String id;
    private final CountDownLatch done = new CountDownLatch(1);
    private volatile boolean stopped;

    // Runtime
    private Context context;
    private DataBase db;
    private StateManager sm;
    private Logger logger;
    private HashMap<String, TrackingInterface> trackers;
    Date current_run_date;
    private boolean shutdown = false;

    // Throughput
    static final int T_ARRAY_SIZE = 20;
    private double tArray[] = new double[T_ARRAY_SIZE];
    private int tArrayCursor = 0;
    private boolean tArrayFilled = false;

    // Data
    private ResultSet rs; // Reuse same for all queries

    public MysqlInput(String id, Configuration config, Context context) throws TrackingException, StateManagerException, DataBaseException {

        // Load config
        this.host = config.get(CONFIG_HOST);
        this.port = config.get(CONFIG_PORT);
        this.user = config.get(CONFIG_USER);
        this.password = config.get(CONFIG_PASSWORD);
        this.dbName = config.get(CONFIG_DBNAME);
        this.options = config.get(CONFIG_OPTIONS);
        this.count_statement = config.get(CONFIG_COUNT_STATEMENT);
        this.statement = config.get(CONFIG_STATEMENT);
        this.page_column = config.get(CONFIG_PAGE_COLUMN);
        this.page_size = config.get(CONFIG_PAGE_SIZE);
        this.sleep = config.get(CONFIG_SLEEP) * 1000;
        this.sleep_page = config.get(CONFIG_SLEEP_PAGE);
        this.clean_run = config.get(CONFIG_CLEAN_RUN);
        this.state_path = config.get(CONFIG_STATE_PATH);
        this.show_in_log = config.get(CONFIG_SHOW_IN_LOG);

        // Plugin helpers
        this.id = id;
        this.context = context;
        this.logger = this.context.getLogger(this);

        // Load trackers
        this.tracking = config.get(CONFIG_TRACKING);
        try {
            this.trackers = Tracking.buildFromMap(this.tracking);
        } catch (TrackingException ex) {
            this.logger.fatal("Tracking columns object parsing failed : " + ex.getMessage());
        }
        String trackerLoadMsg = "\n\n" + "Loaded " + this.trackers.entrySet().size() + " tracking columns :";
        for (Map.Entry<String, TrackingInterface> tracker : this.trackers.entrySet()) {
            trackerLoadMsg += "\n" + tracker.getKey() + " => " + tracker.getValue().toString();
        }
        this.logger.info(trackerLoadMsg + "\n");

        // Welcome message
        this.welcome();

        // DB init
        try {
            this.db = new DataBase(this.logger, this.context, this.host, this.port, this.user, this.password, this.dbName, this.options);
            this.logger.info("Connected to database");
        } catch (DataBaseException ex) {
            this.logger.error("Connection to database failed : " + ex.getMessage());
            this.stop();
            throw ex;
        }

        // State file init
        try {
            this.sm = new StateManager(this.logger, this.state_path, this.trackers, this.clean_run);
            this.clean_run = false;
            this.sm.save();
        } catch (StateManagerException | TrackingException ex) {
            this.logger.error("Failed to initialize state manager : " + ex.getMessage());
            this.stop();
            throw ex;
        }

    }

    private void welcome() {

        this.logger.info("Started mysql_input plugin execution with settings :\n\n"
            + "*** Server ***\n"
            + "host : " + this.host + "\n"
            + "port : " + this.port + "\n"
            + "user : " + this.user + "\n"
            + "password : **********\n"
            + "options : " + this.user + "\n\n"
            + "*** Pipeline ***\n"
            + "count_statement : " + this.count_statement + "\n"
            + "statement : " + this.statement + "\n"
            + "sleep : " + this.sleep / 1000 + "\n"
            + "sleep_page : " + this.sleep_page / 1000.0 + "\n"
            + "page_size : " + this.page_size + "\n"
            + "clean_run : " + (this.clean_run ? "yes" : "no") + "\n"
            + "state_path : " + this.state_path + "\n"
        );

    }

    private ResultsCount getExpectedCounts() throws DataBaseException, TrackingException, SQLException {

        this.logger.info("Preparing count query");

        this.prepareStatement(this.count_statement, null, -1);
        this.injectParams(this.count_statement);
        Date start = new Date();
        ResultSet rs = this.db.runQuery();
        this.logger.info(
            "Count Query [" + ((new Date()).getTime() - start.getTime() / 1000.0) + "s] : \n" + this.db.getQuery(true)
        );
        if (!rs.next()) {
            return null;
        } else {
            return new ResultsCount(rs.getLong(1), rs.getLong(2), rs.getLong(3));
        }

    }

    private void prepareStatement(String statement, ResultsCount rc, long page) throws DataBaseException {

        String stmt;

        if (page == -1) {
            stmt = statement.replace("@mysql_column_paging", "TRUE");
        } else {

            long start = (rc.getMin() + (page - 1) * this.page_size);
            long end = start + this.page_size;
            end = Math.min(end, rc.getMax() + 1);

            stmt = statement.replace("@mysql_column_paging",
                this.page_column + " >= " + start + " AND " + this.page_column + " < " + end
            );
        }

        this.db.prepare(stmt);

    }

    private void injectParams(String statement) throws TrackingException, DataBaseException {

        if (statement.contains(":last_plugin_run_at")) {
            this.sm.setState("last_plugin_run_at", "date", this.current_run_date, true);
            this.db.injectParams("last_plugin_run_at", this.current_run_date);
        }

        if (statement.contains(":current_run_date")) {
            this.db.injectParams("current_run_date", this.current_run_date);
        }

        for (Map.Entry<String, TrackingInterface> tracker : this.trackers.entrySet()) {
            if (statement.contains(":tracked_" + tracker.getKey())) {
                this.db.injectParams("tracked_" + tracker.getKey(), tracker.getValue().getValue());
            }
        }

        // Check the output query has no more parameters
        String finalQuery = this.db.getQuery(false);
        if (finalQuery.contains(" :")) {
            throw new DataBaseException("Some parameters have not been successfully replaced");
        }

    }

    private void consumeAll(Consumer<Map<String, Object>> consumer, ResultsCount rc)
            throws DataBaseException, TrackingException, SQLException, StateManagerException, InterruptedException {

        this.logger.info("Getting all [page_size = " + this.page_size + "]");

        try {
            this.prepareStatement(this.statement, rc, -1);
            this.injectParams(this.statement);
        } catch (DataBaseException ex) {
            this.logger.fatal("Failed query preparation : \n" + this.db.getQuery(true));
            throw ex;
        }

        // Consume
        Date start = new Date();
        try {
            this.rs = this.db.runQuery();
        } catch (DataBaseException ex) {
            this.logger.fatal("Query execution failed : \n" + this.db.getQuery(true));
            throw ex;
        }

        double queryTime = ((new Date()).getTime() - start.getTime()) / 1000.0;

        this.logger.info(
            "\nData Query [" + queryTime
            + "s][No paging] : \n" + this.db.getQuery(true)
        );

        List<Object> rowsLog = this.consumeResults(consumer, this.rs, true);

        this.logger.info(
            "Input throughput : " + this.getThroughput(rowsLog, queryTime) + " rows/s"
            + " (Average " + this.getThroughputAverage() + " rows/s)"
        );

        this.showInLogs(rowsLog, -1, -1);

    }

    private void consumePage(Consumer<Map<String, Object>> consumer, ResultsCount rc, long page, long pageCount)
            throws DataBaseException, TrackingException, SQLException, StateManagerException, InterruptedException {

        this.logger.info("Getting page " + page + " of " + pageCount + " [page_size = " + this.page_size + "]");

        try {
            this.prepareStatement(this.statement, rc, page);
            this.injectParams(this.statement);
        } catch (DataBaseException ex) {
            this.logger.fatal("Failed query preparation : \n" + this.db.getQuery(true));
            throw ex;
        }

        Date start = new Date();
        try {
            rs = this.db.runQuery();
        } catch (DataBaseException ex) {
            this.logger.fatal("Query execution failed : \n" + this.db.getQuery(true));
            throw ex;
        }

        double queryTime = ((new Date()).getTime() - start.getTime()) / 1000.0;

        this.logger.info(
            "Data Query [" + queryTime
            + "s][Page " + page + " of " + pageCount + "] : \n" + this.db.getQuery(true)
        );

        List<Object> rowsLog = this.consumeResults(
            consumer,
            rs, (pageCount - page) < 1 // Last page -> save state ?
        );

        this.logger.info(
            "Input throughput : " + this.getThroughput(rowsLog, queryTime) + " rows/s"
            + " (Average " + this.getThroughputAverage() + " rows/s)"
        );
        this.showInLogs(rowsLog, page, pageCount);

    }

    private List<Object> consumeResults(Consumer<Map<String, Object>> consumer, ResultSet rs, boolean trackState)
            throws SQLException, DataBaseException, TrackingException, StateManagerException, InterruptedException {

        List<Object> showInLogs = new ArrayList<>(); // Show in log
        ResultSetMetaData md = rs.getMetaData();

        // Consume all results
        while (rs.next()) {

            // Handle interruption
            if (this.shutdown) {
              throw new InterruptedException("Shutdown initiated");
            }

            // Build row map
            int columns = md.getColumnCount();
            Map<String, Object> row = new HashMap<>();
            for (int c = 1; c <= columns; c++) {
                row.put(md.getColumnName(c), rs.getObject(c));
            }

            // Record tracker values
            if (trackState) {
                for (Map.Entry<String, TrackingInterface> tracker : this.trackers.entrySet()) {
                    Object value = row.getOrDefault(tracker.getKey(), null);
                    if (value == null) {
                        this.logger.warn("Tracking column [" + tracker.getKey() + "] not found in current resultset. Continuing without...");
                    } else {
                        this.sm.setState(
                                tracker.getKey(),
                                tracker.getValue().getType(),
                                value,
                                true
                        );
                    }
                }
            }

            // Show in log ?
            if (!this.show_in_log.equals("")) {
                Object value = row.getOrDefault(this.show_in_log, null);
                if (value != null) {
                    showInLogs.add(value);
                }
            }

            consumer.accept(row);

        }

        // Cleanup
        if (trackState) {
            this.sm.save();
        } else {
            this.logger.info("State was explicitely requested not to be saved");
        }
        rs.close();

        return showInLogs;

    }

    private void showInLogs(List<Object> showInLogs, long page, long totalPages) {

        // Show in log ?
        if (!this.show_in_log.equals("")) {
            this.logger.info(
                "\n" + (page >= 0 ? "[Page " + page + " of " + totalPages + "] Report\n" : "[No paging used] Report \n")
                + "Processed " + showInLogs.size() + " rows with [" + this.show_in_log + "] : " + Arrays.toString(showInLogs.toArray())
            );
        }

    }

    @Override
    public void start(Consumer<Map<String, Object>> consumer) {

        ResultSet rs;

        try {

            while (!stopped) {

                // Last loop run
                this.current_run_date = new Date();

                // Loop init
                Arrays.fill(this.tArray, 0);
                this.tArrayCursor = 0;
                this.tArrayFilled = false;

                // Get expected count
                ResultsCount rCount;
                try {
                    rCount = this.getExpectedCounts();
                    assert rCount != null;
                } catch (DataBaseException | TrackingException ex) { // Try again next time
                    this.logger.error(ex.getMessage());
                    this.sleep();
                    continue;
                }

                // Have results ?
                if (rCount.getCount() == 0) { // Try again next time
                    this.logger.info("Query produced no results");
                    this.sleep();
                    if (this.shutdown) throw new InterruptedException("Shutdown requested");
                    continue;
                } else {
                    this.logger.info("Count query produced " + rCount.getCount() + " results");
                    this.logger.info("Page column results [" + this.page_column + "] : " + rCount.getMin() + " => " + rCount.getMax());
                }

                // Do we need paging ?
                if (rCount.getCount() <= this.page_size) {

                    // No paging
                    try {
                        this.consumeAll(consumer, rCount);
                    } catch (DataBaseException | StateManagerException ex) {
                        this.logger.error("Consume produced exception : " + ex.getMessage());
                        this.sleep();
                        continue;
                    }


                } else { // Needs paging :-(

                    /*

                    Example 1 :
                    Size = 1000
                    Min = 1
                    Max = 23001
                    => Span = 23000 rows => 23 pages of 1000 rows

                    Example 2 :
                    Size = 1000
                    Min = 1
                    Max = 23561
                    => Span = 23560 rows => 23 pages of 1000 rows + 1 page of 560 rows

                     */
                    long pageCount;
                    double min = rCount.getMin() * 1.0;
                    double max = rCount.getMax() * 1.0;
                    double span = 1 + max - min;
                    if (span % this.page_size > 0) {
                        pageCount = ((long) span / this.page_size) + 1;
                    } else {
                        pageCount = (long) span / this.page_size;
                    }

                    for (long p = 1; p <= pageCount; p++) {

                        try {
                            this.consumePage(consumer, rCount, p, pageCount);
                        } catch (DataBaseException | StateManagerException ex) { // Try again next time
                            this.logger.error("Consume produced exception : " + ex.getMessage());
                            this.sleep();
                            continue;
                        }

                        this.sleepPage();
                        if (this.shutdown) throw new InterruptedException("Shutdown requested");

                    }

                }

                // Sleep until next fetch
                // TODO : keepalive
                this.logger.info(
                    "\n\n****** Batch complete ["
                    + ((new Date()).getTime() - this.current_run_date.getTime()) / 1000.0
                    + "s] No more results to process, waiting for next execution ******");
                this.sleep();

            }

        } catch (SQLException | TrackingException | InterruptedException e) {
            this.logger.error("Main loop exception [" + e.getClass().getName() + "]: " + e.getMessage());
        } finally {
            this.shutdown();
        }
    }

    private void shutdown() {

        try {
            this.db.close();
            this.logger.info("Connection closed");
        } catch (DataBaseException ex) {
            this.logger.info("Connection was already closed");
        }
        stopped = true;
        done.countDown();

    }

    @Override
    public void stop() {
        this.shutdown = true;
    }

    @Override
    public void awaitStop() throws InterruptedException {
        done.await(); // blocks until input has stopped
    }

    @Override
    public Collection<PluginConfigSpec<?>> configSchema() {

        return Arrays.asList(
            CONFIG_HOST,
            CONFIG_PORT,
            CONFIG_USER,
            CONFIG_PASSWORD,
            CONFIG_DBNAME,
            CONFIG_OPTIONS,
            CONFIG_COUNT_STATEMENT,
            CONFIG_STATEMENT,
            CONFIG_PAGE_COLUMN,
            CONFIG_PAGE_SIZE,
            CONFIG_SLEEP,
            CONFIG_SLEEP_PAGE,
            CONFIG_CLEAN_RUN,
            CONFIG_STATE_PATH,
            CONFIG_SHOW_IN_LOG,
            CONFIG_TRACKING
        );

    }

    @Override
    public String getId() {
        return this.id;
    }

    private void sleep() {

        this.logger.info("Sleeping for " + this.sleep / 1000 + " seconds between executions");

        try {
            Thread.sleep(this.sleep);
        } catch (InterruptedException ex) {
            this.logger.info("Interrupter during execution sleep");
            this.shutdown();
        }

    }

    private void sleepPage() {

        this.logger.info("Sleeping for " + this.sleep_page / 1000.0 + " seconds between pages");

        try {
            Thread.sleep(this.sleep_page);
        } catch (InterruptedException ex) {
            this.logger.info("Interrupter during page sleep");
            this.shutdown();
        }

    }

    private double getThroughput(List<Object> rowsList, double time) {

        double result = rowsList.size() / time;

        if (this.tArrayCursor + 1 >= T_ARRAY_SIZE) {
            this.tArrayCursor = 0;
            this.tArrayFilled = true;
        } else {
            this.tArray[++this.tArrayCursor] = result;
        }

        return result;

    }

    private double getThroughputAverage() {

        if (this.tArrayFilled) {
            return Arrays.stream(this.tArray).average().orElse(Double.NaN);
        } else {
            long sum = 0;
            for (int i = 0; i <= this.tArrayCursor; i++) {
                sum += this.tArray[this.tArrayCursor];
            }
            return 1.0 * sum / (this.tArrayCursor + 1);
        }


    }
}
