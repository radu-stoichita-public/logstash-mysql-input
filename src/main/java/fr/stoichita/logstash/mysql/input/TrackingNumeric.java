package fr.stoichita.logstash.mysql.input;

import java.util.HashMap;

public class TrackingNumeric extends Tracking {

    protected Long value;
    protected Long default_value;

    public TrackingNumeric(String type, String column, Long default_value) {
        super(type, column);
        this.default_value = default_value;
    }

    public String getStringValue() {
        return String.valueOf(this.value);
    }

    public Long getValue() {
        return this.value;
    }

    public Long getDefaultValue() {
        return this.default_value;
    }

    public void setInitialValue() {
        this.value = this.default_value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public HashMap<String, Object> toHashMap() {

        HashMap<String, Object> hm = new HashMap<>();
        hm.put("column", this.getColumn());
        hm.put("type", this.getType());
        hm.put("value", this.getValue());
        hm.put("default_value", this.getDefaultValue());
        return hm;

    }

    public String toString() {
        return
            "[Column: " + this.getColumn() +
            ", Type: " + this.getType() +
            ", Default: " + this.getDefaultValue() +
            ", Current: " + this.getValue() + "]";
    }

}
