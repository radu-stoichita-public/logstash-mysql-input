package fr.stoichita.logstash.mysql.input.exception;

public class DataBaseConnectionException extends Exception {
    public DataBaseConnectionException(String msg) {
        super(msg);
    }
}
