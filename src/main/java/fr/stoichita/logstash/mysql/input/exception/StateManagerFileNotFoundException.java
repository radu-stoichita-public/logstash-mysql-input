package fr.stoichita.logstash.mysql.input.exception;

public class StateManagerFileNotFoundException extends Exception {
    public StateManagerFileNotFoundException(String msg) {
        super(msg);
    }
}
