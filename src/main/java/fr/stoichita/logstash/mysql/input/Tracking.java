package fr.stoichita.logstash.mysql.input;

import fr.stoichita.logstash.mysql.input.exception.TrackingException;

import java.text.SimpleDateFormat;
import java.util.*;

abstract public class Tracking implements TrackingInterface {

    protected String column;
    protected String type;

    public Tracking(String type, String column) {

        this.type = type;
        this.column = column;

    }

    public static HashMap<String, TrackingInterface> buildFromMap(Map<String, Object> map) throws TrackingException {

        HashMap<String, TrackingInterface> trackers = new HashMap<>();

        for (Map.Entry<String, Object> tracker : map.entrySet()) {
            if (tracker.getValue() instanceof HashMap) {
                HashMap<String, Object> childrenMap = (HashMap<String, Object>) tracker.getValue();

                String type = (String) childrenMap.get("type");
                TrackingInterface t = null;
                switch (type) {

                    case "numeric":
                        t = new TrackingNumeric(
                            type,
                            tracker.getKey(),
                            (Long) childrenMap.get("default_value")
                        );
                        break;

                    case "string":
                        t = new TrackingString(
                            type,
                            tracker.getKey(),
                            (String) childrenMap.get("default_value")
                        );
                        break;

                    case "date":
                        try {
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date d = formatter.parse((String) childrenMap.get("default_value"));
                            t = new TrackingDate(type, tracker.getKey(), d);
                        } catch (Exception ex) {
                            throw new TrackingException("Failed to parse date tracking settings : " + ex.getMessage());
                        }

                        break;

                }

                trackers.put(tracker.getKey(), t);

            }
        }

        return trackers;

    }

    public String getType() {
        return this.type;
    }

    @Override
    public String getColumn() {
        return this.column;
    }

    public void setValue(String value) throws TrackingException {
        throw new TrackingException("Unsupported setValue with this provider type");
    }

    abstract public void setInitialValue();

    public void setValue(Long value) throws TrackingException {
        throw new TrackingException("Unsupported setValue with this provider type");
    }

    public void setValue(Date value) throws TrackingException {
        throw new TrackingException("Unsupported setValue with this provider type");
    }

    abstract public HashMap<String, Object> toHashMap();

    abstract public String toString();

}
