package fr.stoichita.logstash.mysql.input;

import java.util.HashMap;

public class TrackingString extends Tracking {

    protected String value;
    protected String default_value;

    public TrackingString(String type, String column, String default_value) {
        super(type, column);
        this.default_value = default_value;
    }

    public String getStringValue() {
        return this.value;
    }

    public String getValue() {
        return this.value;
    }

    public String getDefaultValue() {
        return this.default_value;
    }

    public void setInitialValue() {
        this.value = this.default_value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public HashMap<String, Object> toHashMap() {

        HashMap<String, Object> hm = new HashMap<>();
        hm.put("column", this.getColumn());
        hm.put("type", this.getType());
        hm.put("value", this.getValue());
        hm.put("default_value", this.getDefaultValue());
        return hm;

    }

    public String toString() {
        return
            "[Column: " + this.getColumn() +
            ", Type: " + this.getType() +
            ", Default: " + this.getDefaultValue() +
            ", Current: " + this.getValue() + "]";
    }

}
