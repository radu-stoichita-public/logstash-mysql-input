package fr.stoichita.logstash.mysql.input.exception;

public class TrackingException extends Exception {
    public TrackingException(String msg) {
        super(msg);
    }
}
