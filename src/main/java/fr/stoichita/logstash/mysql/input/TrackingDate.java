package fr.stoichita.logstash.mysql.input;

import java.util.Date;
import java.util.HashMap;

public class TrackingDate extends Tracking {

    protected Date value;
    protected Date default_value;

    public TrackingDate(String type, String column, Date default_value) {
        super(type, column);
        this.default_value = default_value;
    }

    public String getStringValue() {
        return new java.sql.Date(this.value.getTime()).toString();
    }

    public Date getValue() {
        return this.value;
    }

    public Date getDefaultValue() {
        return this.default_value;
    }

    public void setInitialValue() {
        this.value = this.default_value;
    }

    public void setValue(Date value) {
        this.value = value;
    }

    public HashMap<String, Object> toHashMap() {

        HashMap<String, Object> hm = new HashMap<>();
        hm.put("column", this.getColumn());
        hm.put("type", this.getType());
        hm.put("value", this.getValue());
        hm.put("default_value", this.getDefaultValue());
        return hm;

    }

    public String toString() {
        return
            "[Column: " + this.getColumn() +
            ", Type: " + this.getType() +
            ", Default: " + this.getDefaultValue().toString() +
            ", Current: " + (this.getValue() != null ? this.getValue().toString() : "null") + "]";
    }

}
