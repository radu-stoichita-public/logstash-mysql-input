package fr.stoichita.logstash.mysql.input;

import co.elastic.logstash.api.Context;
import fr.stoichita.logstash.mysql.input.exception.DataBaseException;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

public class DataBase {

    private Connection dbConn;
    private NamedPreparedStatement dbStmt;

    private Context context;
    private String host;
    private String port;
    private String usr;
    private String pwd;
    private String dbName;
    private String options;

    private final static Executor immediateExecutor = Runnable::run;

    public DataBase(Logger logger, Context context, String host, String port, String usr, String pwd, String dbName, String options) throws DataBaseException {

        this.context = context;
        this.host = host;
        this.port = port;
        this.usr = usr;
        this.pwd = pwd;
        this.dbName = dbName;
        this.options = options;

        int tries = 10;
        while (--tries >= 0) {
            try {
                this.reconnect();
            } catch (DataBaseException ex) {
                logger.error("Could not connect to database (will retry) : " + ex.getMessage());
            }
        }

        if (!this.isConnected()) {
            throw new DataBaseException("Database still not connected after " + (10 - tries) + " tries");
        }

    }

    public boolean isConnected() throws DataBaseException {

        try {
            return !this.dbConn.isClosed();
        } catch (SQLException ex) {
            throw new DataBaseException("Can't check database connection state");
        }

    }

    public void reconnect() throws DataBaseException {

        try {
            DriverManager.setLoginTimeout(10);
            this.dbConn = DriverManager.getConnection(
                "jdbc:mysql://" + this.host + ":" + this.port + "/" + this.dbName + "?" + this.options,
                this.usr,
                this.pwd
            );
        } catch (SQLException ex) {
            throw new DataBaseException("Can't connect to database using information provided : " + ex.getMessage());
        }

    }

    public void close() throws DataBaseException {

        try {
            if (!this.dbConn.isClosed()) {
                this.dbConn.close();
            } else {
                throw new DataBaseException("Connection is not open");
            }
        } catch (SQLException ex) {
            throw new DataBaseException("Can't close database connection : " + ex.getMessage());
        }

    }

    public void prepare(String statement) throws DataBaseException {

        try {
            this.checkConnection();
            this.dbStmt = NamedPreparedStatement.prepareStatement(this.dbConn, statement);
        } catch (SQLException ex) {
            throw new DataBaseException("SQL prepare failed : " + ex.getMessage());
        }

    }

    public void injectParams(String field, Object value) throws DataBaseException {

        try {

            if (value == null) {
                throw new DataBaseException("Injected parameter for field " + field + " is null");
            }

            if (value instanceof Long) {
                this.dbStmt.setLong(field, (long) value);
            } else if (value instanceof String) {
                this.dbStmt.setString(field, (String) value);
            } else if (value instanceof Date) {
                this.dbStmt.setTimestamp(field, new Timestamp(((Date) value).getTime()));
            } else {
                throw new DataBaseException("Injected parameter instance is of unknown type : " + value.getClass().getCanonicalName());
            }

        } catch (SQLException ex) {
            throw new DataBaseException("SQL parameters injection failed : " + ex.getMessage());
        }

    }

    public String getQuery(boolean formatted) {
        return !formatted ?
            this.dbStmt.getQuery()
            : this.dbStmt.getQuery().replaceAll("\\s{2,}", " ");
    }

    public ResultSet runQuery() throws DataBaseException {

        try {
            this.checkConnection();
            this.dbStmt.setQueryTimeout(15);
            this.dbConn.setNetworkTimeout(immediateExecutor, (int) TimeUnit.SECONDS.toMillis(15));
            return this.dbStmt.executeQuery();
        } catch (SQLException ex) {
            throw new DataBaseException("SQL execution failed : " + ex.getMessage());
        }

    }

    public void checkConnection() throws DataBaseException {

        try {
            if (this.dbConn.isClosed()) {
                this.reconnect();
            }
            if (this.dbConn.isClosed()) {
                throw new SQLException("Reconnect failed");
            }
        } catch (SQLException ex) {
            throw new DataBaseException("Database connection lost, could not reconnect : " + ex.getMessage());
        }

    }

}
