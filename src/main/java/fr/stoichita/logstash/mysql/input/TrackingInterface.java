package fr.stoichita.logstash.mysql.input;

import fr.stoichita.logstash.mysql.input.exception.TrackingException;

import java.util.Date;
import java.util.HashMap;

public interface TrackingInterface {

    public String getType();
    public String getColumn();
    public String getStringValue();
    public Object getValue();
    public Object getDefaultValue();
    public void setInitialValue();
    public void setValue(String value) throws TrackingException;
    public void setValue(Long value) throws TrackingException;
    public void setValue(Date value) throws TrackingException;
    public HashMap<String, Object> toHashMap();
    public String toString();

}
