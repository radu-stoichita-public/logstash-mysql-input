package fr.stoichita.logstash.mysql.input.exception;

public class StateManagerException extends Exception {
    public StateManagerException(String msg) {
        super(msg);
    }
}
