package fr.stoichita.logstash.mysql.input;

import fr.stoichita.logstash.mysql.input.exception.StateManagerException;
import fr.stoichita.logstash.mysql.input.exception.StateManagerFileNotFoundException;
import fr.stoichita.logstash.mysql.input.exception.TrackingException;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class StateManager {

    private Logger logger;

    HashMap<String, TrackingInterface> trackers;
    String path;
    JSONObject state;

    public StateManager(Logger logger, String path, HashMap<String, TrackingInterface> trackers, Boolean clean) throws StateManagerException, TrackingException {

        this.trackers = trackers;
        this.path = path;
        this.logger = logger;

        if (clean) {
            try { this.clean(); }
            catch (StateManagerFileNotFoundException ex) { }
        }

        this.read();
        this.setInitialState();

    }

    public void setState(String key, String type, Object value, Boolean includeInQuery) throws TrackingException {

        // Update JSON and trackers
        JSONObject state = new JSONObject();
        state.put("type", type);
        state.put("includeInQuery", includeInQuery);
        TrackingInterface tracker = this.trackers.getOrDefault(key, null);

        if (type.equals("string")) {
            state.put("value", (String) value);
            if (tracker != null) tracker.setValue((String) value);
        }

        if (type.equals("numeric")) {
            state.put("value", (long) value);
            if (tracker != null) tracker.setValue((Long) value);
        }

        if (type.equals("date")) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE);
            state.put("value", formatter.format((Date) value));
            if (tracker != null) tracker.setValue((Date) value);
        }

        this.state.put(key, state);

    }

    public Object getValue(String key) {

        TrackingInterface tracker = this.trackers.getOrDefault(key, null);
        return tracker == null ? null : tracker.getValue();

    }

    public Object getState(String key) throws StateManagerException {

        JSONObject state = !this.state.has(key) ? null : this.state.getJSONObject(key);

        if (state == null) {
            return null;
        }

        try {

            if (state.getString("type").equals("string")) {
                return state.getString("value");
            }

            if (state.getString("type").equals("numeric")) {
                return state.getLong("value");
            }

            if (state.getString("type").equals("date")) {
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE);
                    return formatter.parse(state.getString("value"));
                } catch (ParseException ex) {
                    throw new StateManagerException("Could not parse date field : " + ex.getMessage());
                }

            }

        } catch (Exception e) {
            throw new StateManagerException("Could not get state for key [" + key + "] : " + e.getMessage());
        }


        return null;

    }

    public String getStatesAsString() {
        return this.state.toString();
    }

    private void setInitialState() throws StateManagerException, TrackingException {

        for (Map.Entry<String, TrackingInterface> tracker : this.trackers.entrySet()) {
            if (this.getState(tracker.getKey()) == null) {
                tracker.getValue().setInitialValue();
                this.setState(tracker.getKey(), tracker.getValue().getType(), tracker.getValue().getDefaultValue(), true);
                this.logger.info("Added default state value on field [" + tracker.getKey() + "]");
            } else {
                Object value = null;
                try {
                    JSONObject state = !this.state.has(tracker.getKey()) ? null : this.state.getJSONObject(tracker.getKey());
                    assert state != null;
                    this.logger.info(state.toString());
                    value = this.getState(tracker.getKey());
                    this.setState(tracker.getKey(), tracker.getValue().getType(), value, true);
                    this.logger.info("Added state from file value on field [" + tracker.getKey() + "] : " + value);
                } catch (Exception e) {
                    this.logger.error("Failed to set state from file value on field [" + tracker.getKey() + "] : " + value + " : " + e.getMessage());
                }
            }
        }

    }

    public void save() throws StateManagerException {

        FileWriter fw;

        // Create file if not exists
        if (!this.exists()) {
            throw new StateManagerException("State file still does not exist");
        }

        // Load file contents
        try {
            this.logger.info("Saving state to file (checkpoint) :\n" + state.toString());
            fw = new FileWriter(this.path,false);
            fw.write(state.toString());
            fw.close();
        } catch (IOException ex) {
            throw new StateManagerException("Can't save state file : " + ex.getMessage());
        }

    }

    private void read() throws StateManagerException {

        String contents;

        // Create file if not exists
        if (!this.exists()) {
            this.create();
        }

        // Error if still not exists
        if (!this.exists()) {
            throw new StateManagerException("State file still not exists after creation");
        }

        // Load file contents
        try {
            contents = new String(Files.readAllBytes(Paths.get(this.path)), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            throw new StateManagerException("Can't read state file : " + ex.getMessage());
        }

        // Parse JSON
        try {
            this.state = new JSONObject(contents);
        } catch (JSONException ex) {
            throw new StateManagerException("Can't parse state as JSON : " + ex.getMessage());
        }

        this.logger.info("Loaded state from file : " + state.toString());

    }

    private Boolean exists() {

        File file = new File(this.path);
        return file.exists();

    }

    private void create() throws StateManagerException {

        FileWriter fw;

        try {
            fw = new FileWriter(this.path,false);
            fw.write("{}");
            fw.close();
            this.logger.info("Created new state file");
        } catch (IOException ex) {
            throw new StateManagerException("Failed to create state file");
        }

    }

    private void clean() throws StateManagerFileNotFoundException, StateManagerException {

        File file = new File(this.path);
        if (!file.exists()) {
            throw new StateManagerFileNotFoundException("State file does not exist");
        } else if(!file.delete()) {
            throw new StateManagerException("Can't delete state file");
        }
        this.logger.info("Deleted existing state file (clean_run = true)");

    }

}
